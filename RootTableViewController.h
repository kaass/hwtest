//
//  RootTableViewController.h
//  test
//
//  Created by Konstantin D. on 02.09.15.
//  Copyright (c) 2015 I-Sys. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RootTableViewController : UITableViewController

@end

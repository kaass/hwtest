//
//  RootTableViewController.m
//  test
//
//  Created by Konstantin D. on 02.09.15.
//  Copyright (c) 2015 I-Sys. All rights reserved.
//

#import "RootTableViewController.h"
#import "DetailViewController.h"
#import "NSUserData.h"

@interface NSArray(JSONCategories)
+ (NSArray *)dictionaryWithContentsOfJSONString:(NSString*)path;
@end

@implementation NSArray(JSONCategories)

+ (NSArray *)dictionaryWithContentsOfJSONString:(NSString*)path
{
    NSString * file_path = [[NSBundle mainBundle] pathForResource:[path stringByDeletingPathExtension] ofType:[path pathExtension]];
    NSData * data = [NSData dataWithContentsOfFile:file_path];
    
    __autoreleasing NSError * error = nil;
    
    id result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    
    if (error != nil) return nil;
    
    return result;
}


@end

@interface RootTableViewController ()

@property (nonatomic, strong) NSArray * source_datas;
@property (nonatomic, strong) NSMutableDictionary * section_items;
@property (nonatomic, strong) NSString * section_selected_tag;
@property (nonatomic, strong) NSArray * title_sections;

@end

@implementation RootTableViewController

@synthesize source_datas = _datas;
@synthesize section_items = _items;
@synthesize section_selected_tag = _tag;
@synthesize title_sections = _titles;

- (void)setDataFromJson
{
    self.source_datas = [NSArray dictionaryWithContentsOfJSONString:@"MOCK_DATA.json"];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setDataFromJson];
    
    [self initialization];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.title_sections count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSString *section_title = [self.title_sections objectAtIndex:section];
    return [[self.section_items valueForKey:section_title] count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    static NSString *simpleTableIdentifier = @"scell";
    
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:simpleTableIdentifier];
    }
    
    NSString *section_title = [self.title_sections objectAtIndex:indexPath.section];
    
    NSString *element = [[[self.section_items valueForKey:section_title] objectAtIndex:indexPath.row] fullName];
    NSString *element_detail = [[[self.section_items valueForKey:section_title] objectAtIndex:indexPath.row] job];
//
    cell.textLabel.text = element;
    cell.detailTextLabel.text = element_detail;


//    cell.textLabel.text = @"test";
    return cell;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [self.title_sections objectAtIndex:section];
}

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"cellShow"])
    {
        DetailViewController * destinationVC = segue.destinationViewController;
        
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        NSString * section_title = [self.title_sections objectAtIndex:indexPath.section];
        
        destinationVC.data = [[self.section_items valueForKey:section_title] objectAtIndex:indexPath.row];        
    }
}

-(void)initialization
{
    self.section_selected_tag = @"country";
    NSMutableSet * temp_title_container = [NSMutableSet new];
    
    for (id item in self.source_datas)
    {
        [temp_title_container addObject:[item valueForKey:self.section_selected_tag]];
    }
    
    self.title_sections = [[temp_title_container allObjects] sortedArrayUsingSelector:@selector(compare:)];
    self.section_items = [NSMutableDictionary new];
    
    for(id item in self.title_sections)
    {
        NSArray * section_dic_items = [self.source_datas filteredArrayUsingPredicate:[
                                                                                      NSPredicate predicateWithFormat:@"%K == %@",
                                                                                      self.section_selected_tag, item
                                                                                      ]
                                       ];
        NSMutableArray * objects = [NSMutableArray new];
        
        for(id dic in section_dic_items)
        {
            [objects addObject:[[NSUserData alloc] initWithDictionary:dic]];
        }
        
        [self.section_items setObject:[objects copy] forKey:item];
    }
}

@end

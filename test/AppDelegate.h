 //
//  AppDelegate.h
//  test
//
//  Created by Konstantin D. on 30.08.15.
//  Copyright (c) 2015 I-Sys. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end


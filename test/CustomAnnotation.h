//
//  CustomAnnotation.h
//  test
//
//  Created by kaass on 21.09.15.
//  Copyright © 2015 I-Sys. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface CustomAnnotation : NSObject <MKAnnotation>
@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;
@property (nonatomic, copy) NSString * title;

-(id)initWithTitle:(NSString *)title Location:(CLLocationCoordinate2D)location;
-(MKAnnotationView *)annotationView;

@end

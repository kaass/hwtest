//
//  CustomAnnotation.m
//  test
//
//  Created by kaass on 21.09.15.
//  Copyright © 2015 I-Sys. All rights reserved.
//

#import "CustomAnnotation.h"

@interface UIImage (Resize)
+(UIImage *)imageFrom:(UIImage *)image withNewSize:(CGSize)newSize;
@end

@implementation UIImage(Resize)

+(UIImage *)imageFrom:(UIImage *)image withNewSize:(CGSize)newSize
{
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0.f, 0.f, newSize.width, newSize.height)];
    UIImage * resize_image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return resize_image;
}

@end

@implementation CustomAnnotation

@synthesize title = m_title;
@synthesize coordinate = m_location;

-(id) initWithTitle:(NSString *)title Location:(CLLocationCoordinate2D)location
{
    self = [super init];
    if(self)
    {
        m_title = title;
        m_location = location;
    }
    
    return self;
}

-(MKAnnotationView *)annotationView
{
    MKAnnotationView * annotation_view = [[MKAnnotationView alloc] initWithAnnotation:self reuseIdentifier:@"CustomAnnotation"];
    
    annotation_view.enabled = YES;
    annotation_view.canShowCallout = YES;
    
//    UIImage * image = [UIImage imageNamed:@"images/running_man.png"];
//    CGSize size = CGSizeMake(20.f, 20.f);
    
    
    annotation_view.image = [UIImage imageFrom:[UIImage imageNamed:@"images/running_man.png"]
                                   withNewSize:CGSizeMake(20.f, 20.f)
                             ];
    annotation_view.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    
    return annotation_view;
}



@end

//
//  DetailViewController.h
//  test
//
//  Created by kaass on 04.09.15.
//  Copyright (c) 2015 I-Sys. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NSUserData.h"

@interface DetailViewController : UIViewController

@property (nonatomic, weak) NSUserData * data;

@end

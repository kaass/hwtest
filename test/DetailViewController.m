//
//  DetailViewController.m
//  test
//
//  Created by kaass on 04.09.15.
//  Copyright (c) 2015 I-Sys. All rights reserved.
//

#import "DetailViewController.h"
#import "LocationViewController.h"

@interface DetailViewController ()
{
    IBOutlet UILabel *lblName;
    IBOutlet UILabel *lblLastName;
    IBOutlet UILabel *lblGender;
    IBOutlet UILabel *lblCity;
    IBOutlet UILabel *lblCompany;
    IBOutlet UILabel *lbljob;
    IBOutlet UILabel *lblemail;
    IBOutlet UILabel *lblip;
    
    IBOutlet UIButton *btnLocation;
}
@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    lblCity.text = [self.data city];
    lblName.text = [self.data name];
    lblLastName.text = [self.data lastName];
    lblGender.text = [self.data gender];
    lblCompany.text = [self.data company];
    lbljob.text = [self.data job];
    lblemail.text = [self.data email];
    lblip.text = [self.data ip];
    
    self.navigationItem.title = @"Detail Information";
}

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"locationSeque"])
    {
        LocationViewController * destinationVC = segue.destinationViewController;        
        destinationVC.userLocation = [self.data location];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

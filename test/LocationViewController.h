//
//  LocationViewController.h
//  test
//
//  Created by kaass on 14.09.15.
//  Copyright (c) 2015 I-Sys. All rights reserved.
//

#import <UIKit/UIKit.h>
#include <MapKit/MapKit.h>
#include "NSUserData.h"
@interface LocationViewController : UIViewController <MKMapViewDelegate, CLLocationManagerDelegate>

@property (nonatomic, weak) UserLocation * userLocation;

-(IBAction)setMap:(id)sender;

@end

//
//  LocationViewController.m
//  test
//
//  Created by kaass on 14.09.15.
//  Copyright (c) 2015 I-Sys. All rights reserved.
//

#import "LocationViewController.h"
#import "CustomAnnotation.h"

typedef void (^VoidBlock)(void);

@interface LocationViewController ()
{
    IBOutlet UIButton *btnClose;
    MKMapView *mapView;
}

@property (strong, nonatomic) CLLocationManager *locationManager;
@property (nonatomic, weak) IBOutlet MKMapView *mapView;
@property (nonatomic, copy) VoidBlock exitBlock;
@end


@implementation LocationViewController
@synthesize locationManager = m_location_manager;
@synthesize mapView = m_map_view;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    m_map_view.delegate = self;
    
    self.locationManager = [CLLocationManager new];
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;

    [self.locationManager requestAlwaysAuthorization];
//    if([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
//    {
//        [self.locationManager requestWhenInUseAuthorization];
//    }
    [self.locationManager startUpdatingLocation];
    
    [btnClose addTarget:self action:@selector(closeButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    __weak LocationViewController * weakSelf = self;
    
    self.exitBlock = ^()
    {
        NSLog(@"close");
        [weakSelf dismissViewControllerAnimated:YES completion:nil];
    };
    
    CLLocationCoordinate2D user_location = CLLocationCoordinate2DMake(self.userLocation.latitude, self.userLocation.longitude);
    
    [self adduserLocation:user_location];
//    [self.mapView setCenterCoordinate:user_location animated:YES];
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(user_location, 8000000, 8000000);
    [self.mapView setRegion:[self.mapView regionThatFits:region] animated:YES];
        
    self.mapView.showsUserLocation = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) closeButtonPressed:(UIButton*)button
{
    self.exitBlock();
}

-(IBAction)setMap:(id)sender
{
    switch (((UISegmentedControl *)sender).selectedSegmentIndex)
    {
        case 0:
            m_map_view.mapType = MKMapTypeStandard;
            break;
        case 1:
            m_map_view.mapType = MKMapTypeSatellite;
            break;
        case 2:
            m_map_view.mapType = MKMapTypeHybrid;
            break;
            
        default:
            break;
    }
}

-(void) adduserLocation:(CLLocationCoordinate2D)coordinate
{
    NSLog(@"\nlatitude: %f\nLongitude: %f", coordinate.latitude, coordinate.longitude);
    CustomAnnotation * annotation = [[CustomAnnotation alloc] initWithTitle:@"TestAnnotation" Location:coordinate];
    [self.mapView addAnnotation:annotation];
}

- (MKAnnotationView *)mapView:(MKMapView *)map viewForAnnotation:(id <MKAnnotation>)annotation
{
    if([annotation isKindOfClass:[CustomAnnotation class]])
    {
        CustomAnnotation *custom_annotation = (CustomAnnotation *)annotation;
        
        MKAnnotationView * annotation_view = [map dequeueReusableAnnotationViewWithIdentifier:@"CustomAnnotation"];
        
        if(annotation_view == nil)
            annotation_view = custom_annotation.annotationView;
        else
            annotation_view.annotation = annotation;
        
        return annotation_view;
    }
    else
        return nil;
}

@end

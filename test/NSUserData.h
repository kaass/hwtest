//
//  NSUserData.h
//  test
//
//  Created by kaass on 10.09.15.
//  Copyright (c) 2015 I-Sys. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserLocation : NSObject

@property (nonatomic, assign) double latitude;
@property (nonatomic, assign) double longitude;

-(instancetype) initWithLatitude:(double) latitude andLongitude:(double) longitude;
@end

@interface NSUserData : NSObject

@property (nonatomic, strong, readonly) NSString * name;
@property (nonatomic, strong, readonly) NSString * lastName;
@property (nonatomic, strong, readonly) NSString * fullName;
@property (nonatomic, strong, readonly) NSString * gender;
@property (nonatomic, strong, readonly) NSString * city;
@property (nonatomic, strong, readonly) NSString * company;
@property (nonatomic, strong, readonly) NSString * job;
@property (nonatomic, strong, readonly) NSString * email;
@property (nonatomic, strong, readonly) NSString * ip;
@property (nonatomic, strong, readonly) UserLocation * location;

-(instancetype) initWithDictionary:(NSDictionary *) dic;

@end

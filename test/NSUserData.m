//
//  NSUserData.m
//  test
//
//  Created by kaass on 10.09.15.
//  Copyright (c) 2015 I-Sys. All rights reserved.
//

#import "NSUserData.h"

@implementation UserLocation

@synthesize latitude = m_latitude;
@synthesize longitude = m_longitude;

-(instancetype) initWithLatitude:(double)latitude andLongitude:(double)longitude
{
    self = [super init];
    if(self)
    {
        m_latitude = latitude;
        m_longitude = longitude;
    }
    return self;
}

@end


@implementation NSUserData

@synthesize name = m_name;
@synthesize lastName = m_last_name;
@synthesize fullName = m_full_name;
@synthesize gender = m_gender;
@synthesize city = m_city;
@synthesize company = m_company;
@synthesize job = m_job;
@synthesize email = m_email;
@synthesize ip = m_ip;
@synthesize location = m_location;

-(instancetype) initWithDictionary:(NSDictionary *)dic
{
    self = [super init];
    if(self)
    {
        m_city = [dic objectForKey:@"city"];
        m_name = [dic objectForKey:@"first_name"];
        m_last_name = [dic objectForKey:@"last_name"];
        m_full_name = [dic objectForKey:@"full_name"];
        m_gender = [dic objectForKey:@"gender"];
        m_company = [dic objectForKey:@"company"];
        m_job = [dic objectForKey:@"job"];
        m_email = [dic objectForKey:@"email"];
        m_ip = [dic objectForKey:@"ip_address"];
        m_location = [[UserLocation alloc] initWithLatitude:[[dic objectForKey:@"latitude"] doubleValue]
                                                andLongitude:[[dic objectForKey:@"longtude"] doubleValue]
                      ];
    }
    
    return self;
}

@end
